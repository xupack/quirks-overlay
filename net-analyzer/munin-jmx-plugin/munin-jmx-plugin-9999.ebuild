# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/net-analyzer/munin/munin-1.4.6-r2.ebuild,v 1.4 2011/10/01 03:23:11 phajdan.jr Exp $

EAPI="2"
inherit git-2 java-pkg-2 java-ant-2

DESCRIPTION=""
HOMEPAGE="https://bitbucket.org/xupack/munin-jmx-plugin/overview"
EGIT_REPO_URI="https://bitbucket.org/xupack/munin-jmx-plugin.git"
SRC_URI=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPS=">=net-analyzer/munin-1.4.6[-java]
     "

DEPEND="virtual/jdk:1.6
        ${DEPS}
        "
RDEPEND="virtual/jre:1.6
         ${DEPS}
         "

EANT_BUILD_TARGET="dist"

src_unpack() {
    git-2_src_unpack
}

src_install() {
    insinto "/usr/libexec/munin/"
    doins dist/munin-jmx-ext-plugin.jar

    insinto "/usr/libexec/munin/plugins/"
    doins dist/jmx_ext_
    fperms 777 "/usr/libexec/munin/plugins/jmx_ext_"
}
